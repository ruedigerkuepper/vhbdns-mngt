# vhbdns-mngt

VHB DNS Manager - add, delete and change DNS Records

## setup 

    ./setup.sh 

## usage 

    ./vhbdns -a|-d -z <zone> -s <subdomain> -i <ip> [-t <target> -v]

## add record 

    ./vhbdns -a -z vhbdns.de -s web42 -i 91.105.243.3

## delete record 

    ./vhbdns -d -z vhbdns.de -s web42 -i 91.105.243.3

## create records for deployment targets

    ./vhbdns -a -z vhbdns.de -s web42 -i 91.105.243.3 -t "test" -t "staging" -t "live"

This will create records pointing to 91.105.243.3 for:

- test-web42.vhbdns.de
- staging-web42.vhbdns.de
- live-web42.vhbdns.de

## verbose mode

    ./vhbdns -av -z vhbdns.de -s web42 -i 91.105.243.3 -t "test" -t "staging" -t "live"

Verbose mode will prompt for confirmation before sending the update instructions, the example will show the following output:

    server ns01.9it.de
    zone vhbdns.de
    update add test-web42.vhbdns.de 60 A 91.105.243.3
    update add e2e-web42.vhbdns.de 60 A 91.105.243.3
    update add prod-web42.vhbdns.de 60 A 91.105.243.3
    show
    send
    Use these settings? (y/N) > 
Default action (just pressing ENTER) is "Cancel".
