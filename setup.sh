#!/bin/bash

if [ ! -d keys ]; 
then 
  mkdir keys 
fi 

cd keys

tsig-keygen -a hmac-sha256 ${USER} | tee K${USER}.private

